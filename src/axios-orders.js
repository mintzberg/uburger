import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://uburger-api.firebaseio.com/'
});

export default instance;
